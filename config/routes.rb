Rails.application.routes.draw do
  resources :posts
  resources :comments, only: [:create, :destroy]
  devise_for :users

  authenticated :user do
    root to: 'home#index', as: 'home'
  end

  unauthenticated :user do
    root 'home#front'
  end

  get "profile" => "people#show", :as => "profile"

  get "people" => "people#index", :as => "people"
  post "people" => "people#friends"
  get "userFriends" => "people#userFriends", :as => "userFriends"

  get "share" => "posts#share", :as => "share"

  get "manage" => "controls#index", :as => "manage"
  post "manage" => "controls#manage"

  match :like, to: 'likes#create', as: :like, via: :post
  match :unlike, to: 'likes#destroy', as: :unlike, via: :post

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
