# README


* Ruby version: 3.8.2
* Rails version: 5.0.2
* Database: Sqlite3
* Version Control: Git
* Coding Hosting: Bitbucket

***SETUP***

* To download the repository, use the command git clone.
* After "bundle install" inside the directory of repository (to download dependencies).
* Last, "rails s" for run the web application. For access to PS Social Network, open the web browser and go to "http://localhost:3000".