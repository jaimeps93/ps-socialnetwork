class PeopleController < ApplicationController
  before_action :set_user, :authenticate_user!, except: :front
  respond_to :html, :js

  def index
    @user = current_user
    @requested = @user.requested_friends
    @friends = @user.friends
    @pending = @user.pending_friends
    @users4 = User.where.not(username: current_user.username)
    @users3 = @users4.reject{ |e| @friends.include? e}
    @users2 = @users3.reject{ |e| @requested.include? e}
    @users = @users2.reject{ |e| @pending.include? e}
    @myposts = @user.posts

    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
  end

 def show
    @user = current_user
    @person = User.find_by username: params[:id]
    @posts = Post.where(user_id: @person.id)
    @friends = @person.friends
    @myposts = @person.posts

    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
  end

  def userFriends
    @user = current_user
    @person = User.find_by username: params[:id]
    @requested = @person.requested_friends
    @friends = @person.friends
    @pending = @person.pending_friends
    @users4 = User.where.not(username: @person.username)
    @users3 = @users4.reject{ |e| @friends.include? e}
    @users2 = @users3.reject{ |e| @requested.include? e}
    @users = @users2.reject{ |e| @pending.include? e}
    @myposts = @person.posts

    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
  end

  def friends
    @user = User.find_by username: params[:username]
    if params[:request] != nil
      @friend = User.find_by username: params[:request]
      @user.friend_request(@friend)
    elsif params[:accept] != nil
      @friend = User.find_by username: params[:accept]
      @user.accept_request(@friend)
    elsif params[:decline] != nil
      @friend = User.find_by username: params[:decline]
      @user.decline_request(@friend)
    elsif params[:revoke] != nil
      @friend = User.find_by username: params[:revoke]
      @user.remove_friend(@friend)
    end
    @requested = @user.requested_friends
    @friends = @user.friends
    @pending = @user.pending_friends
    @users4 = User.where.not(username: current_user.username)
    @users3 = @users4.reject{ |e| @friends.include? e}
    @users2 = @users3.reject{ |e| @requested.include? e}
    @users = @users2.reject{ |e| @pending.include? e}
    @myposts = current_user.posts

    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
    redirect_to :back
  end

  private
  def set_user
    @user = current_user
  end
end
