class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?
  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys:[:name, :username, :password_confirmation, :avatar])
    devise_parameter_sanitizer.permit(:sign_in, keys:[:email, :remember_me])
    devise_parameter_sanitizer.permit(:account_update, keys:[:username, :password,
     :password_confirmation, :current_password, :avatar, :remove_avatar])
  end
end
