class PostsController < ApplicationController
  before_action :set_post, :authenticate_user!, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json

  def share
    @post = Post.find_by id: params[:id]
    @post2 = Post.new
    @post2.id = (Post.last.id)+1
    @post2.attachment = @post.attachment
    @user = User.find_by id: @post.user_id
    @post2.content = "" + @user.username + " wrote: " + @post.content + ""
    @post2.user_id = current_user.id
    t = Time.now
    @post2.created_at = "" + t.strftime("%Y-%m-%d %H:%M:%S") + ""
    @post2.updated_at = "" + t.strftime("%Y-%m-%d %H:%M:%S") + ""
    @post2.save
    redirect_to :back
  end

  def create
    @post = Post.new(post_params) do |post|
      post.user = current_user
    end
    if @post.save
      redirect_to root_path
    else
      redirect_to root_path, notice: @post.errors.full_messages.first
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    @post.update(post_params)
    redirect_to root_path
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:attachment, :content, :user_id)
    end
end
