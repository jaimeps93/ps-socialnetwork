class ControlsController < ApplicationController
  before_action :set_user, :authenticate_user!, except: :front
  respond_to :html, :js

  def index
    @user = current_user
    @users = User.where.not(username: current_user.username)
    @myposts = @user.posts
    @friends = @user.friends
    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
  end

  def manage
    @user = current_user
    @myposts = @user.posts
    @friends = @user.friends
    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
    if params[:profile] != nil
      redirect_to controller: 'people', action: 'show', id: params[:profile], something: 'else'
    elsif params[:relationship] != nil
      redirect_to controller: 'people', action: 'userFriends', id: params[:relationship], something: 'else'
    elsif params[:delete] != nil
      User.where(username: params[:delete]).destroy_all
      redirect_to :back
    elsif params[:role] != nil
      #user = User.where(username: params[:role])
      @user = User.find_by username: params[:role]
      if @user.admin == true
        @user.admin = false
      else
        @user.admin = true
      end
      @user.save
      redirect_to :back
    end
  end

  private
  def set_user
    @user = current_user
  end
end
