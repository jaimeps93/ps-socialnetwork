class HomeController < ApplicationController
  before_action :set_user, except: :front
  respond_to :html, :js

  def index
    @user = current_user
    @friends = @user.friends
    @post = Post.new
    @ids = @friends.pluck(:id)
    @posts = (Post.where(user_id: @ids))+@user.posts
    @myposts = @user.posts

    @likes = 0
    @myposts.each do |post|
      @likes += post.get_likes.size
    end
  end

  def front
  end

  private
  def set_user
    @user = current_user
  end
end
